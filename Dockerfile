FROM node:16-alpine as builder 

ENV NODE_ENV build

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000

CMD ["npm", "run", "start" ]

