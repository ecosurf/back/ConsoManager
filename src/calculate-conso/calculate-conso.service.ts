import { Injectable } from '@nestjs/common';
import { Requests } from 'src/entities/requests';
import { ConsoInfo } from 'src/dto/conso.dto';
import { CreateRequestsDto } from 'src/dto/requests.dto';
import { RequestsService } from 'src/requests/requests.service';


@Injectable()
export class CalculateConsoService {
    constructor(private _requestsService: RequestsService) { }

    public async convertToCountryCode(country: string): Promise<number> {
        let COUNTRY_CODE = {
            "FRANCE": 250,
            "GERMANY": 276
        };

        if (!COUNTRY_CODE[country]) {
            return 0;
        }

        return COUNTRY_CODE[country]
    }

    private async createRequest(userId: number, consoInfo: ConsoInfo, url: string): Promise<Requests> {

        let requestDto = new CreateRequestsDto(
            consoInfo.server_conso_data,
            consoInfo.server_conso_energy,
            await this.convertToCountryCode(consoInfo.server_country),
            url,
            new Date(),
            new Date(),
            new Date(),
            userId,
            1
        )

        let request: Requests;

        try {
            request = await this._requestsService.create(
                requestDto
            )
        } catch (error) {
            return null;
        }

        return request;
    }

    public async normalizeUrl(url: string): Promise<string> {
        url = url.replace("www.", "");
        return url
    }

    public async getCountryEmissionPerKwh(country: string) {
        let COUNTRY_EMISSIONS_PER_KWH = {
            "FRANCE": 50,
            "GERMANY": 500
        };
        return await COUNTRY_EMISSIONS_PER_KWH[country.toUpperCase()]
    }

    public async calculateConsoFromurlMock(url_target: string, userId: number): Promise<any> {
        let website_mocked = {
            "youtube.com": {
                "server_conso_data": 0.25,
                "server_conso_energy": 0,
                "server_country": "France",
                "category_id": 2
            },
            "netflix.com": {
                "server_conso_data": 0.7,
                "server_conso_energy": 0,
                "server_country": "France",
                "category_id": 3
            },
            "fr.wikipedia.org": {
                "server_conso_data": 0.004,
                "server_conso_energy": 0,
                "server_country": "France",
                "category_id": 4
            },
            "primevideo.com": {
                "server_conso_data": 0.3,
                "server_conso_energy": 0,
                "server_country": "Germany",
                "category_id": 3
            },
            "dailymotion.com": {
                "server_conso_data": 0.4,
                "server_conso_energy": 0,
                "server_country": "France",
                "category_id": 1
            },
            "intensif05.ensicaen.fr:8443": {
                "server_conso_data": 0,
                "server_conso_energy": 0,
                "server_country": "France",
                "category_id": 5
            },
            "intensif05.ensicaen.fr": {
                "server_conso_data": 0,
                "server_conso_energy": 0,
                "server_country": "France",
                "category_id": 5
            }
        };

        for (const website in website_mocked) {
            let conso_info = website_mocked[website];
            let server_conso_emission = await this.getCountryEmissionPerKwh(conso_info["server_country"]) * conso_info["server_conso_data"];

            website_mocked[website]["server_conso_energy"] = server_conso_emission / 1000;
        }


        let result: ConsoInfo;

        url_target = await this.normalizeUrl(url_target);

        if (website_mocked[url_target]) {
            result = website_mocked[url_target];
        } else {
            result = {
                "server_conso_data": Math.random() * 0.1,
                "server_conso_energy": 0,
                "server_country": Math.random() < 0.5 ? "France" : "Germany",
                "category_id": Math.floor(Math.random() * 5) + 1
            };

            let server_conso_emission = await this.getCountryEmissionPerKwh(result["server_country"]) * result["server_conso_data"];
            result["server_conso_energy"] = server_conso_emission / 1000;
        }

        if (userId > 0) {
            await this.createRequest(userId, result, url_target);
        }

        return result;
    }

    public async calculateConsoFromurl(url_target: string, userId: number): Promise<any> {
        return await this.calculateConsoFromurlMock(url_target, userId);
    }


    public async calculateConso(target_url, userId): Promise<any> {
        let conso_test_request = {
            "url_target": target_url,
            "data_used": 0.001,
            "route": {
                "abc.com": {
                    "server_conso": 0.001,
                    "country": "France"
                },
                "def.com": {
                    "server_conso": 0.003,
                    "country": "Germany"
                }
            }
        }

        let emission = await this.computeEmission(conso_test_request);

        return await this.calculateConsoFromurl(target_url, userId);
    }

    public async computeEmission(json): Promise<number> {
        let data_used = json.data_used;
        let emission = 0;

        await Promise.all(Object.keys(json.route).map(async (key) => {
            let server_conso = json.route[key].server_conso;
            let country = json.route[key].country;

            let country_emission = await this.getCountryEmissionPerKwh(country);
            emission += data_used * server_conso * country_emission;
        }));

        return emission;
    }
}