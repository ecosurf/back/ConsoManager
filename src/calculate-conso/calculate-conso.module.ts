import { Module } from '@nestjs/common';
import { RequestsProviders } from 'src/requests/requests.providers';
import { RequestsService } from 'src/requests/requests.service';
import { UsersProviders } from 'src/users/users.providers';
import { UsersService } from 'src/users/users.service';
import { CalculateConsoController } from './calculate-conso.controller';
import { CalculateConsoService } from './calculate-conso.service';

@Module({
  controllers: [CalculateConsoController],
  providers: [CalculateConsoService, UsersService, ...UsersProviders, RequestsService, ...RequestsProviders]
})
export class CalculateConsoModule {

}
