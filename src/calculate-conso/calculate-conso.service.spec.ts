import { Test, TestingModule } from '@nestjs/testing';
import { CalculateConsoService } from './calculate-conso.service';

describe('CalculateConsoService', () => {
  let service: CalculateConsoService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CalculateConsoService],
    }).compile();

    service = module.get<CalculateConsoService>(CalculateConsoService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
