import { Controller, Get, Req, Res, Param } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import { CalculateConsoService } from './calculate-conso.service';

@Controller('conso')
export class CalculateConsoController {
    constructor(private _calculateConsoService: CalculateConsoService, private _usersService: UsersService) { }

    @Get()
    public async getConso() {
        return "Get conso api"
    }

    @Get('calculate')
    public async calculateConso(@Req() req, @Res() res): Promise<void> {
        let target_url = req.query.url;

        if (!target_url) {
            res.status(400).send("Missing mandatory url parameter");
        }

        let result = await this._calculateConsoService.calculateConso(target_url, -1);
        res.status(200).send(result);
    }

    @Get('website/:userId')
    public async getWebsiteConso(@Req() req, @Res() res): Promise<void> {

        let url = req.query.url;
        let userId = req.params.userId;

        if (!url) {
            res.status(400).send("Missing mandatory url parameter");
        }

        // create a user following user dto
        // let userDto = new CreateUsersDto(
        //     'firstName',
        //     'lastName',
        //     new Date(),
        //     new Date(),
        //     1
        // )

        // let userCreated = await this._usersService.create(userDto);

        let userFound = await this._usersService.findOne(userId);


        if (!userFound) {
            res.status(400).send("User not found with userId " + userId);
        }

        let result = await this._calculateConsoService.calculateConsoFromurl(url, userId);

        res.status(200).send({
            "url": url,
            "conso": result,
            "userFound": userFound.toJSON()
        })
    }
}
