import { Test, TestingModule } from '@nestjs/testing';
import { CalculateConsoController } from './calculate-conso.controller';

describe('CalculateConsoController', () => {
  let controller: CalculateConsoController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CalculateConsoController],
    }).compile();

    controller = module.get<CalculateConsoController>(CalculateConsoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
