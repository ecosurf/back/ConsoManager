import {
  Model,
  Table,
  Column,
  DataType,
  Index,
  Sequelize,
  ForeignKey,
} from 'sequelize-typescript';

export interface categoriesAttributes {
  id?: number;
  name: string;
  code: string;
  createdAt: Date;
  updatedAt: Date;
}

@Table({ tableName: 'Categories', timestamps: false })
export class Categories
  extends Model<categoriesAttributes, categoriesAttributes>
  implements categoriesAttributes
{
  @Column({ primaryKey: true, autoIncrement: true, type: DataType.INTEGER })
  @Index({ name: 'PRIMARY', using: 'BTREE', order: 'ASC', unique: true })
  id?: number;

  @Column({ type: DataType.STRING(255) })
  name!: string;

  @Column({ type: DataType.STRING(255) })
  code!: string;

  @Column({ type: DataType.DATE })
  createdAt!: Date;

  @Column({ type: DataType.DATE })
  updatedAt!: Date;
}
