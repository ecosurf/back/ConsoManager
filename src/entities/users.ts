import {
  Model,
  Table,
  Column,
  DataType,
  Index,
  Sequelize,
  ForeignKey,
} from 'sequelize-typescript';
import { EntityRepository, Repository } from 'typeorm';

export interface usersAttributes {
  id?: number;
  firstName: string;
  lastName: string;
  createdAt: Date;
  updatedAt: Date;
  thresoldId?: number;
}

@Table({ tableName: `Users`, timestamps: false })
export class Users
  extends Model<usersAttributes, usersAttributes>
  implements usersAttributes {
  @Column({ primaryKey: true, autoIncrement: true, type: DataType.INTEGER })
  @Index({ name: 'PRIMARY', using: 'BTREE', order: 'ASC', unique: true })
  id?: number;

  @Column({ type: DataType.STRING(255) })
  firstName!: string;

  @Column({ type: DataType.STRING(255) })
  lastName!: string;

  @Column({ type: DataType.DATE })
  createdAt!: Date;

  @Column({ type: DataType.DATE })
  updatedAt!: Date;

  @Column({ field: 'ThresoldId', allowNull: true, type: DataType.INTEGER })
  @Index({ name: 'ThresoldId', using: 'BTREE', order: 'ASC', unique: false })
  thresoldId?: number;
}

