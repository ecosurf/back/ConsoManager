import {
  Model,
  Table,
  Column,
  DataType,
  Index,
  Sequelize,
  ForeignKey,
} from 'sequelize-typescript';

export interface requestsAttributes {
  id?: number;
  quantityData: number;
  quantityCo2: number;
  codeCountry: number;
  url: string;
  date: Date;
  createdAt: Date;
  updatedAt: Date;
  userId?: number;
  categoryId?: number;
}

@Table({ tableName: 'Requests', timestamps: false })
export class Requests
  extends Model<requestsAttributes, requestsAttributes>
  implements requestsAttributes
{
  @Column({ primaryKey: true, autoIncrement: true, type: DataType.INTEGER })
  @Index({ name: 'PRIMARY', using: 'BTREE', order: 'ASC', unique: true })
  id?: number;

  @Column({ type: DataType.INTEGER })
  quantityData!: number;

  @Column({ field: 'quantityCO2', type: DataType.INTEGER })
  quantityCo2!: number;

  @Column({ type: DataType.INTEGER })
  codeCountry!: number;

  @Column({ type: DataType.STRING(255) })
  url!: string;

  @Column({ type: DataType.DATE })
  date!: Date;

  @Column({ type: DataType.DATE })
  createdAt!: Date;

  @Column({ type: DataType.DATE })
  updatedAt!: Date;

  @Column({ field: 'UserId', allowNull: true, type: DataType.INTEGER })
  @Index({ name: 'UserId', using: 'BTREE', order: 'ASC', unique: false })
  userId?: number;

  @Column({ field: 'CategoryId', allowNull: true, type: DataType.INTEGER })
  @Index({ name: 'CategoryId', using: 'BTREE', order: 'ASC', unique: false })
  categoryId?: number;
}
