import {
  Model,
  Table,
  Column,
  DataType,
  Index,
  Sequelize,
  ForeignKey,
} from 'sequelize-typescript';

export interface thresoldsAttributes {
  id?: number;
  name: string;
  code: string;
  price: number;
  value: number;
  createdAt: Date;
  updatedAt: Date;
}

@Table({ tableName: 'Thresolds', timestamps: false })
export class Thresolds
  extends Model<thresoldsAttributes, thresoldsAttributes>
  implements thresoldsAttributes
{
  @Column({ primaryKey: true, autoIncrement: true, type: DataType.INTEGER })
  @Index({ name: 'PRIMARY', using: 'BTREE', order: 'ASC', unique: true })
  id?: number;

  @Column({ type: DataType.STRING(255) })
  name!: string;

  @Column({ type: DataType.STRING(255) })
  code!: string;

  @Column({ type: DataType.INTEGER })
  price!: number;

  @Column({ type: DataType.INTEGER })
  value!: number;

  @Column({ type: DataType.DATE })
  createdAt!: Date;

  @Column({ type: DataType.DATE })
  updatedAt!: Date;
}
