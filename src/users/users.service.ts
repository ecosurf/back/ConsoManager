import { Injectable, Inject } from '@nestjs/common';
import { CreateUsersDto } from '../dto/users.dto';
import { Users } from '../entities/users';


@Injectable()
export class UsersService {
    constructor(
        @Inject('USERS_REPOSITORY')
        private usersRepository: typeof Users,
    ) {}

    async create(createUsersDto: CreateUsersDto): Promise<Users> {
        const user = createUsersDto.createUsersDto();
        return this.usersRepository.create<Users>(user.dataValues);
    }

    async findOne(id: number): Promise<Users> {
        return this.usersRepository.findOne<Users>({ where: { id } });
    }

    async findAll(): Promise<Users[]> {
        return this.usersRepository.findAll<Users>();
    }
}
