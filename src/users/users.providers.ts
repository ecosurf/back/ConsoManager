import { Users } from '../entities/users'

export const UsersProviders = [
    {
        provide: 'USERS_REPOSITORY',
        useValue: Users,
    },
];
