import { Sequelize } from 'sequelize-typescript';
import { Users } from '../entities/users';
import { Requests } from '../entities/requests';
import { Categories } from '../entities/categories';
import { Thresolds } from '../entities/thresolds'

export const databaseProviders = [
    {
        provide: 'SEQUELIZE',
        useFactory: async () => {
            const sequelize = new Sequelize({
                dialect: 'mysql',
                host: 'database',
                port: 3306,
                username: 'root',
                password: 'Kq^H#GFP4f#Zbsy#',
                database: 'user',
            });
            sequelize.addModels([Users, Requests, Categories, Thresolds]);
            await sequelize.sync();
            return sequelize;
        },
    },
];