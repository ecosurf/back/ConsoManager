import { Injectable, Inject } from '@nestjs/common';
import { CreateRequestsDto } from 'src/dto/requests.dto';
import { Requests } from '../entities/requests';


@Injectable()
export class RequestsService {
    constructor(    
        @Inject('REQUESTS_REPOSITORY')
        private usersRepository: typeof Requests,
    ) {}

    async create(createRequestDto: CreateRequestsDto): Promise<Requests> {
        const user = createRequestDto.createRequestsDto();
        return this.usersRepository.create<Requests>(user.dataValues);
    }

    async findOne(id: number): Promise<Requests> {
        return this.usersRepository.findOne<Requests>({ where: { id } });
    }

    async findAll(): Promise<Requests[]> {
        return this.usersRepository.findAll<Requests>();
    }
}
