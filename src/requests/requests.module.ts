import { Module } from '@nestjs/common';
import { RequestsService } from './requests.service';
import { RequestsProviders } from './requests.providers';
import { DatabaseModule } from '../database/database.module';

@Module({
  imports: [DatabaseModule],
  controllers: [],
  providers: [
    RequestsService,
    ...RequestsProviders,
  ],
})
export class UsersModule {}  