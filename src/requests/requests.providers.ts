import { Requests } from '../entities/requests'

export const RequestsProviders = [
    {
        provide: 'REQUESTS_REPOSITORY',
        useValue: Requests,
    },
];
