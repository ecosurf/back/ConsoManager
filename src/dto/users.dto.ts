import { Users } from '../entities/users';

export class CreateUsersDto {
    
    public id !: number;
    public firstName !: string;
    public lastName !: string;
    public createdAt !: Date;
    public updatedAt !: Date;
    public thresoldId !: number;

    constructor(firstName: string, lastName: string, createdAt: Date, updatedAt: Date, thresoldId: number) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.thresoldId = thresoldId;
    }

    public createUsersDto() {
        return new Users({
            firstName: this.firstName,
            lastName: this.lastName,
            createdAt: this.createdAt,
            updatedAt: this.updatedAt,
            thresoldId: this.thresoldId
        })
    }
}