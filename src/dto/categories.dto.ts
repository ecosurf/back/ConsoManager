import { Categories } from '../entities/categories';

export class CreateCategoriesDto {

    public id !: number;
    public name !: string;
    public code !: string;
    public createdAt !: Date;
    public updatedAt !: Date;

    constructor(id: number, name: string, code: string, createdAt: Date, updatedAt: Date) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public createCategoriesDto() {
        return new Categories({
            id: this.id,
            name: this.name,
            code: this.code,
            createdAt: this.createdAt,
            updatedAt: this.updatedAt
        })
    }
}