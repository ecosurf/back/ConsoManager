export class ConsoInfo {
    public server_conso_data!: number;

    public server_conso_energy!: number;

    public server_country!: string;

    public category_id!: number;

    constructor(server_conso_data: number, server_conso_energy: number, server_country: string, category_id: number) {
        this.server_conso_data = server_conso_data;
        this.server_conso_energy = server_conso_energy;
        this.server_country = server_country;
        this.category_id = category_id;
    }
}