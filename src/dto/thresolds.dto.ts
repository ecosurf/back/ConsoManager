import { Thresolds } from '../entities/thresolds';

export class CreateThresoldsDto {
    
        public id !: number;
        public name !: string;
        public code !: string;
        public price !: number;
        public value !: number; 
        public createdAt !: Date;
        public updatedAt !: Date;
    
        constructor(id: number, name: string, code: string, price: number, value: number, createdAt: Date, updatedAt: Date) {
            this.id = id;
            this.name = name;
            this.code = code;
            this.price = price;
            this.value = value;
            this.createdAt = createdAt;
            this.updatedAt = updatedAt;
        }

        public createThresoldsDto() {
            return new Thresolds({
                id: this.id,
                name: this.name,
                code: this.code,
                price: this.price,
                value: this.value,
                createdAt: this.createdAt,
                updatedAt: this.updatedAt
            })
        }
    }