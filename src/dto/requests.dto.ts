import { Requests } from '../entities/requests';

export class CreateRequestsDto {
    public id !: number;
    public quantityData !: number;
    public quantityCo2 !: number;
    public codeCountry !: number;
    public url !: string;
    public date !: Date;
    public createdAt !: Date;
    public updatedAt !: Date;
    public userId !: number;
    public categoryId !: number;

    constructor(quantityData: number, quantityCo2: number, codeCountry: number, url: string, 
                date: Date, createdAt: Date, updatedAt: Date, userId: number, categoryId: number) {
        this.quantityData = quantityData;
        this.quantityCo2 = quantityCo2;
        this.codeCountry = codeCountry;
        this.url = url;
        this.date = date;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.userId = userId;
        this.categoryId = categoryId;
    }

    public createRequestsDto() {
        return new Requests({
            quantityData: this.quantityData,
            quantityCo2: this.quantityCo2,
            codeCountry: this.codeCountry,
            url: this.url,
            date: this.date,
            createdAt: this.createdAt,
            updatedAt: this.updatedAt,
            userId: this.userId,
            categoryId: this.categoryId
        })
    }   

}